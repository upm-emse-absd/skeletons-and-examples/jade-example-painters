package es.upm.emse.absd;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

public final class Utils {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private static int cidCnt = 0;
    private static String cidBase;

    /**
     * Generate a Conversation ID for identifying messages.
     * @param agent the initialising agent of the conversation.
     * @return the CID.
     */
    public static String genCID(Agent agent)
    {
        if (cidBase==null) {
            cidBase = agent.getLocalName() + agent.hashCode() +
                System.currentTimeMillis()%10000 + "_";
        }
        return  cidBase + (cidCnt++);
    }

    /**
     * Returns an ACLMessage with content ready to be sent to a destination.
     * @param origin the initialising agent of the conversation.
     * @param perf the message performative.
     * @param content the message.
     * @param dest identification of the destination agent.
     * @return the message ready to be sent.
     */
    public static ACLMessage newMsg(Agent origin, int perf, String content, AID dest)
    {
        ACLMessage msg = newMsg(origin, perf);
        if (dest != null) msg.addReceiver(dest);
        msg.setContent( content );
        return msg;
    }

    /**
     * Returns an ACLMessage for a concrete Agent with a concrete performative and an unique CID
     * @param origin the initialising agent of the conversation.
     * @param perf the message performative.
     * @return the configured message */
    public static ACLMessage newMsg(Agent origin, int perf)
    {
        ACLMessage msg = new ACLMessage(perf);
        msg.setConversationId(genCID(origin));
        return msg;
    }

    /**
     * Returns true if the agent is registered in the DFService.
     * @param agent the agent.
     * @param type the service.
     * @return true if the agent is registered, false otherwise.
     */
    public static boolean register(Agent agent, String type)
    {
        ServiceDescription sd  = new ServiceDescription();
        sd.setType(type);
        sd.setName(agent.getLocalName());
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(agent.getAID());
        dfd.addServices(sd);

        try {
            DFService.register(agent, dfd );
            return true;
        }
        catch (FIPAException fe) {
            fe.printStackTrace();
            return false;
        }
    }

    /**
     * Deregister the agent from all attached services.
     * @param agent the agent.
     */
    public static void deregister(Agent agent) {
        try { DFService.deregister(agent); }
        catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * The agent obtains a list of agents that offer a concrete service.
     * @param agent the agent who searches in the yellow pages
     * @param service the service.
     * @return the ID array of agents attached to the service.
     */
    public static AID[] searchDF(Agent agent, String service)
    {
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType( service );
        dfd.addServices(sd);

        try
        {
            DFAgentDescription[] result = DFService.search(agent, dfd);
            AID[] agents = new AID[result.length];
            for (int i=0; i<result.length; i++)
                agents[i] = result[i].getName();
            return agents;

        }
        catch (FIPAException e) { return null; }

    }


}
