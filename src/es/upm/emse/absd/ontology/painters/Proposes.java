package es.upm.emse.absd.ontology.painters;


import jade.content.*;
import jade.util.leap.*;
import jade.core.*;

/**
 * Protege name: Proposes
 *
 * @author ontology bean generator
 * @version 2010/04/12, 14:03:55
 */
public class Proposes implements Predicate {

    /**
     * Protege name: estimation
     */
    private Estimation estimation;

    public void setEstimation(Estimation value) {
        this.estimation = value;
    }

    public Estimation getEstimation() {
        return this.estimation;
    }

}
