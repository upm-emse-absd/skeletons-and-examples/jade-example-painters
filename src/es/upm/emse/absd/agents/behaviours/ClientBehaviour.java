package es.upm.emse.absd.agents.behaviours;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.ontology.painters.Estimation;
import es.upm.emse.absd.ontology.painters.PaintServOntology;
import es.upm.emse.absd.ontology.painters.Proposes;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.extern.java.Log;

@Log
public class ClientBehaviour extends SimpleBehaviour {

    Integer currentPrice = Integer.MAX_VALUE;
    AID painter = null;
    boolean finished = false;

    // Codec for the SL language used and instance of the ontology
    // PaintServOntology that we have created
    private final Codec codec = new SLCodec();
    private final Ontology ontology = PaintServOntology.getInstance();

    @Override
    public void action() {
        log.info(this.getAgent().getLocalName() + ": Managing proposals...");
        ACLMessage msg = this.getAgent().receive(
                MessageTemplate.and(
                        MessageTemplate.MatchLanguage(codec.getName()),
                        MessageTemplate.MatchOntology(ontology.getName())));
        if (msg!=null) clientLogic(msg);
        else if (painter!=null) choosePainter();
    }

    private void clientLogic(ACLMessage msg) {
        if (msg.getPerformative() == ACLMessage.PROPOSE) {
            Integer newPrice = extractMsgOntoContent(msg);
            if (newPrice != null && currentPrice > newPrice) {
                log.info(this.getAgent().getLocalName() + ": I receive a better offer from " + msg.getSender().getLocalName());
                currentPrice = newPrice;
                painter = msg.getSender();
            }
        } else {
            log.info(this.getAgent().getLocalName() + ": Ehmmmmmmmmmmm... OK?");
        }
    }

    private Integer extractMsgOntoContent(ACLMessage msg) {
        try {
            // The ContentManager transforms the content (string) in java objects
            ContentElement ce = this.getAgent().getContentManager().extractContent(msg);
            if (ce instanceof Proposes prop) {
                // If an estimation has arrived, it is analysed
                Estimation est = prop.getEstimation();
                return est.getAmount();
            }
        } catch (Codec.CodecException | OntologyException e) {
            log.warning(this.getAgent().getLocalName()+": something went wrong with the ontology...\n" + e.getMessage());
        }
        return null;
    }

    private void choosePainter() {
        ACLMessage reply = Utils.newMsg(this.getAgent(), ACLMessage.ACCEPT_PROPOSAL, null, painter);
        reply.setOntology(ontology.getName());
        reply.setLanguage(codec.getName());
        this.getAgent().send(reply);
        log.info(this.getAgent().getLocalName() + ": I choose " + painter.getLocalName() + " with " + currentPrice + "€" );
        finished = true;
    }

    @Override
    public boolean done() {
        return finished;
    }

}
