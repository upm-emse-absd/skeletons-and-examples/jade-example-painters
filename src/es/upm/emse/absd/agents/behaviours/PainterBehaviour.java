package es.upm.emse.absd.agents.behaviours;

import es.upm.emse.absd.ontology.painters.Estimation;
import es.upm.emse.absd.ontology.painters.EstimationRequest;
import es.upm.emse.absd.ontology.painters.PaintServOntology;
import es.upm.emse.absd.ontology.painters.Proposes;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lombok.extern.java.Log;

@Log
public class PainterBehaviour extends CyclicBehaviour {

    int price;

    // Codec for the SL language used and instance of the ontology
    // PaintServOntology that we have created
    private final Codec codec = new SLCodec();
    private final Ontology ontology = PaintServOntology.getInstance();

    public void action() {
        // Waits for estimation requests
        ACLMessage msg = this.getAgent().blockingReceive(
                MessageTemplate.and(MessageTemplate.MatchLanguage(codec.getName()),
                        MessageTemplate.MatchOntology(ontology.getName()))
        );
        painterLogic(msg);
    }

    private void painterLogic(ACLMessage msg) {
        switch (msg.getPerformative()) {
            case ACLMessage.CFP -> {
                if (isEstimationRequest(msg)) {
                    log.info(this.getAgent().getLocalName() + ": New proposal!!!");
                    this.getAgent().send(sendOntoAmount(msg));
                }
            }
            case ACLMessage.ACCEPT_PROPOSAL ->
                    log.info(this.getAgent().getLocalName() + ": I'm the chosen one with " + this.price + "€");
            default -> {
                ACLMessage response = msg.createReply();
                log.info(this.getAgent().getLocalName() + ": Ehmmmmmmmmmmm... What are you talking about?");
                response.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                this.getAgent().send(response);
            }
        }
    }

    private ACLMessage sendOntoAmount(ACLMessage msg) {
        ACLMessage reply = msg.createReply();
        reply.setPerformative(ACLMessage.PROPOSE);
        reply.setLanguage(codec.getName());
        reply.setOntology(ontology.getName());

        Proposes prop = new Proposes();
        Estimation estimation = new Estimation();
        estimation.setAmount(price = genEstimation());
        prop.setEstimation(estimation);

        try {
            getAgent().getContentManager().fillContent(reply, prop);
        } catch (Codec.CodecException | OntologyException e) {
            log.warning(this.getAgent().getLocalName()+": something went wrong with the ontology...\n" + e.getMessage());
        }

        return reply;

    }

    private boolean isEstimationRequest(ACLMessage msg) {
        // The ContentManager transforms the message content (string) in java objects
        try {
            ContentElement ce = getAgent().getContentManager().extractContent(msg);
            // We expect an action inside the message
            if (ce instanceof Action agAction) {
                Concept conc = agAction.getAction();
                // If the action is EstimationRequest...
                return conc instanceof EstimationRequest;
            }
        } catch (Codec.CodecException | OntologyException e) {
            log.warning(this.getAgent().getLocalName()+": something went wrong with the ontology...\n" + e.getMessage());
        }
        return false;
    }

    private static int genEstimation() {
        // A random estimation between 475 and 525 is calculated
        double sign = Math.random();
        double range = Math.random();

        if (sign < 0.5D)
            range *= -1D;
        return (int)(500 + range * 25D);
    }

}
