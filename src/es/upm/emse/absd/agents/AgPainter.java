/*****************************************************************
 Agent offering the painting service: AgPainter.java
 *****************************************************************/
package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.agents.behaviours.PainterBehaviour;
import es.upm.emse.absd.ontology.painters.PaintServOntology;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import lombok.extern.java.Log;

/**
 * This agent has the following functionality:
 * <ul>
 * <li> It registers itself in the DF as PAINTER
 * <li> Waits for requests of its painting service
 * <li> If any estimation request arrives, it answers with a random value
 * <li> Finally, it waits for the client answer
 * </ul>
 *
 * @author Ricardo Imbert, Jose Barambones, UPM
 * @version $Date: 2023/03/08 $ $Revision: 1.0 $
 **/


@Log
public class AgPainter extends Agent {

    public final static String PAINTER = "Painter";

    // Codec for the SL language used and instance of the ontology
    // PaintServOntology that we have created
    private final Codec codec = new SLCodec();
    private final Ontology ontology = PaintServOntology.getInstance();

    protected void setup() {

        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        log.info(getLocalName() + ": has entered into the system");
        Utils.register(this, PAINTER);

        addBehaviour(new PainterBehaviour());

    }

}								
