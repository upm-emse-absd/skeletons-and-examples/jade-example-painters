/*****************************************************************
Agent Client demanding a painting service: AgClient.java

 *****************************************************************/
package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.agents.behaviours.ClientBehaviour;
import es.upm.emse.absd.ontology.painters.EstimationRequest;
import es.upm.emse.absd.ontology.painters.PaintServOntology;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

/**
 * This agent has the following functionality: 
 * <ul>
 * <li> It register itself in the DF
 * <li> Asks the DF the name of new "painter" agents
 * <li> If there is any, it ask this agent for a service estimation;
 *      if not, waits and try it again later
 * <li> Waits for an answer to the request
 * <li> If the estimation provided is the best one among the received in a minute time,
 *      it sends an acceptation message to the painter; if not, sends one of rejection
 * </ul>
 * @author Ricardo Imbert, Jose Barambones, UPM
 * @version $Date: 2023/03/08 $ $Revision: 1.0 $
 **/

@Log
public class AgClient extends Agent {

    public final static String PAINTER = "Painter";

	// Codec for the SL language used and instance of the ontology
	// PaintServOntology that we have created
	private final Codec codec = new SLCodec();
	private final Ontology ontology = PaintServOntology.getInstance();



	protected void setup()
	{
		log.info(getLocalName()+": has entered into the system. Waiting for painter registering...");

		// Register of the codec and the ontology to be used in the ContentManager
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);

		SequentialBehaviour steps = new SequentialBehaviour();
		steps.addSubBehaviour(new WakerBehaviour(this, 5000) {
			@Override
			protected void onWake() {
				super.onWake();
				AID[] painters = Utils.searchDF(this.getAgent(), PAINTER);
				assert painters != null;
				for (AID painter : painters) {
					send(newMsgWithOnto(painter));
				}
			}

			private ACLMessage newMsgWithOnto(AID destination) {
				ACLMessage newMsg = Utils.newMsg(this.getAgent(),ACLMessage.CFP,null,destination);
				newMsg.setLanguage(codec.getName());
				newMsg.setOntology(ontology.getName());
				EstimationRequest er = new EstimationRequest();
				// As it is an action and the encoding language the SL, it must be wrapped into an Action.
				Action agAction = new Action(destination,er);
				try {
					getContentManager().fillContent(newMsg, agAction);
				} catch (Codec.CodecException | OntologyException ce) {
					log.warning(getLocalName()+": something went wrong with the ontology...");
					log.warning(ce.getMessage());
				}
				return newMsg;
			}
		});
		steps.addSubBehaviour(new ClientBehaviour());

		addBehaviour(steps);

	}

}
